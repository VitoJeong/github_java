import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class FrameEx2 extends Frame{
	// 패널 : 여러개의 컴포넌트를 그룹별로 묶어서 사용하는 컨테이너/컴포넌트
	// -> 기본 레이아웃 : FlowLayout을 사용
	Panel p1,p2,p3;
	
	
	public FrameEx2(){
		super("Frmae v1.2");
		
		// 보더레이아웃 적용
		setLayout(new BorderLayout());
		
		// 패널 객체 생성
		p1 = new Panel();
		p2 = new Panel();
		p3 = new Panel();
		// 북-버튼1
		p1.add(new Button("Button 01"));
		p1.add(new Button("Button 01-1"));
		
		// 센터-버튼1
		p2.add(new Button("Button 02"));
		
		Button btn3 = new Button("Button 03");
		// 남 - 버튼1
		p3.add(btn3,BorderLayout.SOUTH);
		p3.add(new Button("Button 03-1"),BorderLayout.SOUTH);
		
		p1.setBackground(Color.CYAN);
		p2.setBackground(Color.lightGray);
		p3.setBackground(Color.blue);
				
		// 프레임에 추가
		add(p1,BorderLayout.NORTH);
		add(p2,"Center");
		add(p3,BorderLayout.SOUTH);
		
		// 프레임의 공간에 한번에 여러개의 컴포넌트를 추가
		// -> 패널 객체를 생성 -> 패널에 컴포넌트 추가 -> 추가된 패널을 프레임에 추가
		
		btn3.setVisible(false);
		
		setSize(400,200);
		setVisible(true);
		
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				// 프레임창 닫기
				dispose();
				// 프로세스 종료(작업관리자 -> 작업끝내기)
				System.exit(0);
			}
		});
	}
}
public class Test3 {

	public static void main(String[] args) {
		new FrameEx2();
	}

}
