import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Test2 {

	public static void main(String[] args) {

		int data = 0;
		
		// System객체는 키보드에 접근하여 입력값을 받아옴
		// System.in 표준 입력 반환받음
		InputStream myIn = System.in;
		
		OutputStream myOut = System.out;
		try{
			while((data = myIn.read()) != -1){
				// System.out.println((char)data);
				// myOut.print(data);
				// print()를 사용못함 outputstream이 printstream을 상속하지 않기때문에
				myOut.write(data); // 강제형변환 필요없음, 한글안깨짐
			}
		} catch(IOException e){
			System.out.println("입력 관련 오류 발생!");
			e.printStackTrace();
		}
		
		// OutputStream 클래스
		// 1) 추상클래스, 2) 바이트 출력 스트림의 최상위 클래스
		
	} // main()

} // Test2 



