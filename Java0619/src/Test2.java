
class Gugudan extends Thread{
	
	int dan;
	public Gugudan() {}
	public Gugudan(int dan) {
		this.dan = dan;
	}
	@Override
	public void run() {
			
			// Thread를 하나씩 작동하기 위해 sleep()
			long time = (long)(Math.random()*500);
			
			System.out.println(dan+"단 "+time+"만큼 sleep()");
			
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("<"+dan+"단>");
			for(int j=1;j<=9;j++){
				System.out.println(dan+" * " + j +" = "+ dan*j);
			}
		
	}
	
	
}


public class Test2 {

	public static void main(String[] args) {
		
		// 구구단을 출력(세로)
		// 2~9단(랜덤한 순서로 출력)
		// -> 구구단 객체를 생성해서 출력
		for(int i=2;i<10;i++){
			Thread dan = new Gugudan(i);
			dan.start();
		}
		
	}

}
