import java.util.Scanner;

class Runner extends Thread{
	
	private String rname="";
	int meter=0;
	
	public Runner(){}
	public Runner(String rname){
		this.rname = rname;
	}
	
	public void run(){
		
		for(int i=0;i<10;i++){
			
			meter+=10;
			
			if(meter == 100){
				System.out.println("도착 ! " + rname);
			}else{
				System.out.println(rname+"주자"+meter+"M 도착.");
			}
			
			try {
				Thread.sleep((int)Math.random()*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		} // for
		
	} // run()
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	
	
}



public class Test1 {

	public static void main(String[] args) {
		// 쓰레드를 사용한 달리기 경주
		
		// 몇명이서 달리기를 할건지 입력
		// 각각의 주자가 10m마다 본인의 위치를 출력
		// 해당 주자가 100m에 도착하면 "도착" 달리기 완료
		
		// 주자(Runner)
		// 이름,위치(meter)
		// 0~100m 까지 이동 -> run()
		// sleep() 메서드를 사용해서 10m마다 사용(다른값)
		Scanner sc = new Scanner(System.in);
		
		System.out.println("달릴 사람의 수를 입력하세요 >>");
		int runner = sc.nextInt();
		
		for(int i=1;i<=runner;i++){
			Thread run = new Runner("runner NO."+i);
			run.start();
		}

	}

}
