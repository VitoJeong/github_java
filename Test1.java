import java.io.IOException;

public class Test1 {

	public static void main(String[] args) throws IOException {
		// 자바 입,출력(IO) -> java.io 패키지에서 제공
		// 스트림(Stream) : 연속된 일련의 데이터
		// 모든 자바에서의 동작은 스트림 기반으로 처리가 됨.
		// 노드(node) : 스트림의 목적지
		
		// 표준 입력 장치 -> 키보드, (in)
		// 표준 출력 장치 -> 모니터 (out)
		
		// 1) 바이트 스트림 : 숫자형태의 데이터가 전달, 바이트, 바이트 배열, 정수 값으로 
		//				InputStream, OutputStream
		
		// 2) 텍스트 스트림 : 문자형태(Character기반)의 데이터가 전달, 문자, 문자 배열, 문자열 
		//				Read, Writer
		
		// InputStream클래스
		// 1) 추상클래스 2)바이트 입력 스트림 중에서 최상위 클래스
		
		// InputStream myInput = new InputStream();
		
		int data = 0;
		System.out.println("키보드 값 입력하시오>");
		
		// ctrl + z -> 입력의 끝(-1)
		while((data = System.in.read()) != -1){ // -1 더 이상 입력이 없는 경우
			
			System.out.println("입력값 : "+(char)data); 
			// 명시적으로 형변환을 해줘야만 데이터를 확인할 수 있다.
		}
		
	}

}
