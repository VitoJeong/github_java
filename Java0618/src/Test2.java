class ThreadPri extends Thread{
	
	public ThreadPri(){}
	public ThreadPri(String name,int p){
		super(name);
		setPriority(p); // Thread우선순위 부여
	}
	@Override
	public void run() {
		for(int i=0;i<5;i++){
			System.out.println(i+ "-"+getName()+" 우선순위 : "+getPriority());
		}
	}
	
	
	
}
public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ThreadPri t1 = new ThreadPri("작업", Thread.MIN_PRIORITY);
		ThreadPri t2 = new ThreadPri("중요작업", Thread.MAX_PRIORITY);
		ThreadPri t3 = new ThreadPri("보통작업", Thread.NORM_PRIORITY);
		
		t1.start();
		try {
			t1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t2.start();
		t3.start();
				
	}

}
