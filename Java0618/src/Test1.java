class TestThread implements Runnable{
	
	int count = 0;
	int num = 0;
	
	// 생성자를 사용해서 쓰레드가 도착할 최종 목적지(count의 크기)
	// 초기화
	public TestThread(){}
	public TestThread(int cnt){
		count = cnt;
	}
	
	@Override
	public void run() {
		
		// 무한루프
		// num 1씩 증가 -> count 크기까지 증가
		// 쓰레드의 이름 : num의 값을 출력
		while(true){
			
			try {
				// Thread를 번갈아가면서 실행시키기위해 일정시간 대기
				Thread.sleep(100); // 1/1000초
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			num++;
			System.out.println(Thread.currentThread().getName()+ " : " + num);
			
			if(num >=count) break;
			
		}

	}
	
}

public class Test1 {

	public static void main(String[] args) {
		// 메인메서드 제외 3개의 쓰레드를 번갈아가면서 작동시켜서 해당목표를 찾아가는 동작
		TestThread tr = new TestThread(50);
		
		// runnable 인터페이스를 구현한 객체
		Thread th1 = new Thread(tr,"1번 쓰레드");
		// thread객체 -> thread객체가 있어야만 start()사용가능
		th1.start();
		TestThread tr2 = new TestThread(15);
		Thread th2 = new Thread(tr2,"2번 쓰레드");
		th2.start();
		
		try {
			// 해당 Thread를 완전히 실행할 수  있도록 처리(다른 Thread대기)
			th2.join(); 
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TestThread tr3 = new TestThread(33);
		Thread th3 = new Thread(tr3,"3번 쓰레드");
		th3.start();
		

	}

	
	
	
}




