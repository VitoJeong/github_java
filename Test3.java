import java.io.*;

public class Test3 {
	
	
	public static void main(String[] args) {
		// 키보드 입력을 바이트 스트림의 형태로 입력받아서
		// 화면에 출력하는 프로그램을 작성
		// * InputStream, OutputStream객체를 사용
		// * 혹시나 키보드 입력값 중에서 'x' 'X' 있을경우 프로그램 종료
		// * 종료된 시점에 총 입력된 글자 수 를 출력
		
		int data = 0;
		int sum = 0;
		InputStream is = System.in;
		OutputStream os = System.out;
		
			try {
				while((data=is.read()) != -1){
					
					if(data=='x' || data == 'X')
						break;
				
						os.write(data);
						sum++;
					
				}
			} catch (IOException e) {
				System.out.println("입력관련오류발생!");
				e.printStackTrace();
			}
			
			System.out.println("총 입력된 글자 : "+sum+"개");

	}

}
