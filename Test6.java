import java.io.*;

public class Test6 {

	public static void main(String[] args) throws FileNotFoundException {
		// 파일 복사 프로그램 생성(COPY)
		// 특정 파일을 선택해서 바탕화면에 이름 _copy 복사본 파일을 생성
		// * 복사한 파일의 크기 출력(콘솔창)
		
		// 파일 읽어서 다른 파일로 출력
		
		int data=0;
		int size=0;
		String InPath = "C:\\Users\\ITWILL\\Desktop\\hello.txt";
		String OutPath = "C:\\Users\\ITWILL\\Desktop\\hello_copy.txt";
		InputStream fis =null;
		OutputStream fos =null;
		
		
		try{
			fis = new FileInputStream(InPath);
			fos = new FileOutputStream(OutPath);
			while((data = fis.read()) != -1){ 
				fos.write(data);
				size++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			try {
				fis.close();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
		}
		
		System.out.println("복사파일 크기" + size+"byte");
		
	}

	
	
}




